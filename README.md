# README #

General purpose makefile for compiling C++ projects

### What is this repository for? ###

The repository contains a makefile and auxiliary files, which can be used to compile C++ projects. The makefile creates .d dependency files as well, which will result in the recompilation of source files, when included header files are modified. The makefile also creates include -I arguments for all directories containing header files. This allows developers to include any header file in any source file: e.g. e.g. using 
```
#!c++

#include "some_header.hpp"
```
 instead of 
```
#!c++

#include "../../someDir/someOtherDir/some_header.hpp"
```


### How do I get set up? ###

* Copy the makefiles into the root of your project
* The makefile expects to find all source and header files in the **src** directory. Either create the directory, or modify the makefile and set the *SRC_DIR* to your root source directory 

### How do i use it? ###

The makefile contains several targets:

* all - Used to compile source files into object files, then the object files into a final executable, which has the same name as project root directory
* clean - Cleans all compiled files and their directories