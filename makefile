
##################################################################################################################################
#                                       General purpose makefile for compiling C++ projects                                      #
#................................................................................................................................#
#   Author: Lorand Varga                                                                                                         #
#                                                                                                                                #
#   Description: This makefile can be used to compile C++ projects, which respectes the following requirements:                  #
#       * Source files are contained in the 'src' directory                                                                      #
#                                                                                                                                #
#   Use: The target 'all' will invoke the target for compiling .o files, which will be stored in 'bin/obj', which are then       #
#        compiled into an executable with the same name as the project directory, and will be created in the 'bin' directory     #
#        The 'clean' target will delete all compiled files and their containing directories                                      #
#        The 'print' target is used to debugging purposes                                                                        #
#................................................................................................................................#
#                                                                                                                                #
#                                                     Enjoy!                                                                     #
##################################################################################################################################


#	Compiler
CC := g++

#	Project directories
SRC_DIR := src
BIN_DIR := bin
OBJ_DIR := $(BIN_DIR)/obj

#	List of source files and compiled files within the project (using relative paths)
SRC_FILES := $(shell find $(SRC_DIR) -type f \( -name "*.c" -or -name "*.cpp" \))
OBJ_FILES := $(subst $(SRC_DIR),$(OBJ_DIR),$(SRC_FILES:%.cpp=%.o))
DEP_FILES := $(OBJ_FILES:%.o=%.d)

#	Name of the directory containing this project
CURRENT_DIR = $(notdir $(shell pwd))


#	Main target of the makefile: The executable with the same name as the project directory
TARGET := $(BIN_DIR)/$(CURRENT_DIR)

#	Find all header files, extract their directory paths, and create -I include arguments for the compiler
#	This allows developers to include any header file in any source file without the need to use relative paths:
#		e.g. #include "some_header.hpp" instead of #include "../../someDir/someOtherDir/some_header.hpp"
HED_FILES := $(shell find $(SRC_DIR) -type f \( -name "*.h" -or -name "*.hpp" \))
HED_DIRS := $(sort $(dir $(HED_FILES)))
INC_ARGS := $(addprefix "-I", $(HED_DIRS))


#	Auxiliary make targets can be inlcuded here. The two auxilary files make.defines and make.libs are only included if they exist

#	Get varibale DEF_ARGS, contianing a list of -D arguments for the compiler, used for creating preprocessor directives
-include defines.mk

#	Get varibale LIB_ARGS, containing a list of -L and -l arguments for the compiler, used for linking external libraries
-include libs.mk

#	Argument lists for the compiler:
#		* W_ARGS - arguments used for warnings
#		* C_ARGS - arguments used for compiling .o files from source files
#		* L_ARGS - arguments used for linking .o files into the final executable binary 
W_ARGS := -Wall -Wextra
C_ARGS := $(W_ARGS) -c -g -MMD -std=c++11 $(INC_ARGS) $(DEF_ARGS) $(LIB_ARGS) 
L_ARGS := $(W_ARGS) -g -std=c++11 $(INC_ARGS) $(LIB_ARGS)


#	Colors used for pretty 'echo' messages. Use with 'echo -e'
COLOR_DEFAULT :=\e[39m
COLOR_YELLOW :=\e[33m
COLOR_GREEN :=\e[32m
COLOR_MAGENTA :=\e[35m
COLOR_LIGHT_BLUE :=\e[94m
COLOR_RED :=\e[91m


#	Main target
all: $(TARGET)


#	Clean and build main target
rebuild: clean all


#	Target used for makefile debugging purposes
print: 
	@echo -e "$(COLOR_LIGHT_BLUE)Printing makefile variables$(COLOR_DEFAULT)"
	@echo "CURRENT_DIR=$(CURRENT_DIR)"
	@echo "TARGET=$(TARGET)"
	@echo "SRC_FILES=$(SRC_FILES)"
	@echo "HED_FILES=$(HED_FILES)"
	@echo "OBJ_FILES=$(OBJ_FILES)"
	@echo "DEP_FILES=$(DEP_FILES)"
	@echo "INC_ARGS=$(INC_ARGS)"
	@echo "DEF_ARGS=$(DEF_ARGS)"
	@echo "LIB_ARGS=$(LIB_ARGS)"
	@echo "W_ARGS=$(W_ARGS)"
	@echo "C_ARGS=$(C_ARGS)"
	@echo "L_ARGS=$(L_ARGS)"
	

#	Target for creating final executable from object files
$(TARGET): $(OBJ_FILES)
	@echo -e "$(COLOR_MAGENTA)Bulding target: $@ $(COLOR_DEFAULT)"
	@mkdir -p $(dir $@)
	@$(CC) $(L_ARGS) $^ -o $@


#	Include .d dependecy files if they exists. These files are created in the same directory as .o files, 
#	using the -MMD compiler argument
-include $(DEP_FILES)


#	Target for creating .o files from the source files. This target also creates .d dependency files, 
#	using the -MMD compiler argument
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	@echo -e "$(COLOR_GREEN)Compiling $< --> $@ $(COLOR_DEFAULT)"
	@mkdir -p $(dir $@)
	@$(CC) $(C_ARGS) $< -o $@


#	Target used for cleaning the project of all compiled files and their directories
clean:
	@echo -e "$(COLOR_YELLOW)Cleaning project $(COLOR_DEFAULT)"
	@rm -rf $(BIN_DIR)

	