
##################################################################################################################################
#                                        Auxiliray makefile containing lists of libraries                                        #
#................................................................................................................................#
#   Author: Lorand Varga                                                                                                         #
#                                                                                                                                #
#   Description: This makefile can be used to add third party libraries to the c++ project                                       #
#                                                                                                                                #
#	Use: LIB_ARGS will contain the list of arguments used to add libraries during compile time                                   #
#................................................................................................................................#
#                                                                                                                                #
#                                                     Enjoy!                                                                     #
##################################################################################################################################

#	Library files
LIB_FILES :=

#	Libary directories
LIB_DIRS := 

#	Create arguments for the compiler
LIB_ARGS := $(addprefix "-I", $(LIB_DIRS))
LIB_ARGS += $(addprefix "-i", $(LIB_FILES))