
##################################################################################################################################
#                                        Auxiliray makefile containing lists of defines                                          #
#................................................................................................................................#
#   Author: Lorand Varga                                                                                                         #
#                                                                                                                                #
#   Description: This makefile can be used to add defines to the c++ project                                                     #
#                                                                                                                                #
#	Use: DEF_ARGS will contain the list of arguments used to add defines during compile time                                     #
#................................................................................................................................#
#                                                                                                                                #
#                                                     Enjoy!                                                                     #
##################################################################################################################################

#	List of defines and their values
DEFINES := DEBUG=1

#	Create arguments for the compiler
DEF_ARGS := $(addprefix "-D", $(DEFINES)) 